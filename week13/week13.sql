#load data infile '/tmp/datas/movie_stars.csv'
#into table movie_stars fields terminated by ',';

#show variables like 'secure_file_priv';

#hangi ülkelerden kaç müşteri var onu listeler
select Country, count(CustomerID)
from Customers
group by Country;

select ShipperName,count(Orders.OrderID) as numberOfOrders
from Shippers join Orders on Shippers.ShipperID=Orders.ShipperID
group by ShipperName
order by numberOfOrders desc;