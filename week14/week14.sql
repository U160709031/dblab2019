load data infile "C:\\ProgramData\\MySQL\\MySQL_Server5.7\\Uploads\\movies.csv"
into table movies fields terminated by ',' enclosed by ' " ';

load data infile "C:\\ProgramData\\MySQL\\MySQL_Server5.7\\Uploads\\movie_stars.csv"
into table movie_stars fields terminated by ',' ;

load data infile "C:\\ProgramData\\MySQL\\MySQL_Server5.7\\Uploads\\stars.csv"
into table stars fields terminated by ',';

load data infile "C:\\ProgramData\\MySQL\\MySQL_Server5.7\\Uploads\\countries.csv"
into table countries fields terminated by ',';

load data infile "C:\\ProgramData\\MySQL\\MySQL_Server5.7\\Uploads\\directors.csv"
into table directors fields terminated by ',';

load data infile "C:\\ProgramData\\MySQL\\MySQL_Server5.7\\Uploads\\genres.csv"
into table genres fields terminated by ',';

load data infile "C:\\ProgramData\\MySQL\\MySQL_Server5.7\\Uploads\\languages.csv"
into table languages fields terminated by ',';

load data infile "C:\\ProgramData\\MySQL\\MySQL_Server5.7\\Uploads\\movie_directors.csv"
into table movie_directors fields terminated by ',';

load data infile "C:\\ProgramData\\MySQL\\MySQL_Server5.7\\Uploads\\producer_countries.csv"
into table producer_countries fields terminated by ',';


show variables like "secure_file_priv";